#configuracion interfaz de red

WINDOWS

Nos vamos a Inicio > buscamos **POWERSHELL** y lo ejecutamos cómo **administrador**.

Ejecutamos el siguiente comando: netsh

Navegamos hasta el apartado de **interface**:
netsh> interface

Mostramos las interfaces que tenemos disponibles:
netsh interface>show interface

Nos aparecerá que tenemos una llamada «Conexión de área local», le cambiaremos el nombre a una que nos sea más sencilla, en mi caso **eth0:**

set interface "Conexión de área local" newname="eth0"

Si queremos que nuestra dirección ip sea estática, ejecutamos el siguiente comando:
netsh interface>ipv4 set address eth0 static X.X.X.X (dirección ip) X.X.X.X (máscara de subred) X.X.X.X (puerta de enlace)

Procedemos a establecer un servidor de DNS dinámico:
set dnsservers eth0 source=dhcpSi nos aparece algún fallo indicado por netsh, tranquilos, se habrá escrito en la tabla de direcciones aún apareciendo el fallo. 

También podemos deshabilitar/habilitar nuestra interfaz, por si queremos reiniciarla, con el siguiente comando:
set interface eth0 enable

Si queremos volver atrás de algún apartado, ejecutamos los dos puntos:
netsh interface>..
netsh>



LINUX	

Configurar interfaz de red en Linux

A continuación veremos como configurar las interfaces de red desde la consola, tanto para IPs estáticas como dinámicas, y en sistemas derivados de Red Hat o Debian

Configurar IP dinámica
Cuando configuramos un host con IP dinámica, el router se encargará de asignar una IP (variable) a la tarjeta de red conectada.

RedHat / Fedora /CentOS
$ sudo cat /etc/sysconfig/network-scripts/ifcfg-eth0
HWADDR=88:AE:1D:69:5A:5A
TYPE=Ethernet
BOOTPROTO=dhcp
DEFROUTE=yes
PEERDNS=yes
PEERROUTES=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
IPV6_FAILURE_FATAL=no
NAME=eth0
UUID=2efbd6c0-e3d5-45af-9631-10635fd6a39a
ONBOOT=yes
Debian / Ubuntu / Linux Mint
$ sudo cat /etc/network/interfaces
iface eth0 inet dhcp
 

Configurar IP estática
Si por el contrario queremos decidir nosotros que IP debe de recibir cada host/dispositivo de la red, podremos configurar la interfaz de modo que siempre conserve/reciba la misma dirección IP.

RedHat / Fedora /CentOS
$ sudo cat /etc/sysconfig/network-scripts/ifcfg-eth0
DEVICE="eth0"
BOOTPROTO="static"
IPADDR="192.168.1.32"
NETMASK="255.255.255.0"
NETWORK="192.168.1.0"
BROADCAST="192.168.1.255"
GATEWAY="192.168.1.1"
ONBOOT="yes"
Nota: En determinados sistemas es posible que eth0 (nombre de la interfaz a nivel de sistema) cambie por otro (enp1s0, wireless…).

Debian / Ubuntu / Linux Mint
$ sudo cat /etc/network/interfaces
iface eth0 inet static
address 192.168.1.23
network 192.168.1.0
netmask 255.255.255.0
broadcast 192.168.1.255
gateway 192.168.1.1
hwaddress 81:ab:1c:59:aa:7b
Nota: En sistemas que hacen uso de NetworkManager para controlar la red, podemos encontrar el archivo /etc/NetworkManager/system-connections/<nombre-conexión> en el que se almacena la configuración de la interfaz:

[802-3-ethernet]
mac-address=88:AE:1D:69:5A:5A

[connection]
id=wired-11
uuid=212274c7-08bc-4586-9h49-c1u218p9239f
type=802-3-ethernet
timestamp=1424866869

[ipv6]
method=auto

[ipv4]
method=manual
dns=8.8.8.8;8.8.4.4;
address1=192.168.1.50/24,192.168.1.1

